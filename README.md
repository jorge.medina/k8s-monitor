# K8s Demonstration

## Introducción

Este repositorio almacena código de infraestructura y documentación que sirven
como guía para desplegar clusters Kubernetes.

### Objetivos

Describir los componentes que conforman la solución, estos son playbooks de
despliegues y documentación de uso:

* kubernetes: En este directorio estan los playbooks y scripts para despliegues.

## Kubernetes

Los módulos para despliegue son:

* externalmodules: Scripts de despliegue de bootstrap de registry, helm,
  nginx-ingress.
* elasticsearch: Scripts y playbooks para desplegar almacenamiento persistente y elasticsearch.
* mongo: Scripts y playbooks para desplegar almacenamiento persistente y mongodb.
