#!/bin/bash

source ../extramodules/vars/allvars

replaceVars() {
  sed -i \
    -e "s^{{TEMPLATE_APP_NAME}}^$TEMPLATE_APP_NAME^g" \
    -e "s^{{TEMPLATE_PORT}}^$TEMPLATE_PORT^g" \
    -e "s^{{TEMPLATE_CONTAINER_PORT}}^$TEMPLATE_CONTAINER_PORT^g" \
    -e "s^{{TEMPLATE_DOCKER_REPO}}^$TEMPLATE_DOCKER_REPO^g" \
    -e "s^{{SECRET_NAME}}^$SECRET_NAME^g" \
    -e "s^{{TEMPLATE_HOST}}^$TEMPLATE_HOST^g" \
    playbooks/deployment.yaml \
    playbooks/ingress.yaml
}

deploy() {
  kubectl apply -f playbooks/deployment.yaml
  kubectl apply -f playbooks/ingress.yaml
}

# main
replaceVars
deploy

exit 0
