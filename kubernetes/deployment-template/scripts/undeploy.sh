#!/bin/bash

undeploy() {
  kubectl delete -f playbooks/deployment.yaml
  kubectl delete -f playbooks/ingress.yaml
}

# main
undeploy

exit 0
