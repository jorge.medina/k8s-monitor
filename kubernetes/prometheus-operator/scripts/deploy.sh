#!/bin/bash

source ../extramodules/vars/allvars

installPrometheusOperator() {
  CMD=$(helm ls --all 'prometheus' | wc -l)
  if [[ "$CMD" == 0 ]]; then
    helm repo update
    printf "\n==> Install prometheus-operator CRDs \n"
    kubectl apply -f https://raw.githubusercontent.com/coreos/prometheus-operator/master/example/prometheus-operator-crd/alertmanager.crd.yaml
    kubectl apply -f https://raw.githubusercontent.com/coreos/prometheus-operator/master/example/prometheus-operator-crd/prometheus.crd.yaml
    kubectl apply -f https://raw.githubusercontent.com/coreos/prometheus-operator/master/example/prometheus-operator-crd/prometheusrule.crd.yaml
    kubectl apply -f https://raw.githubusercontent.com/coreos/prometheus-operator/master/example/prometheus-operator-crd/servicemonitor.crd.yaml
    kubectl apply -f https://raw.githubusercontent.com/coreos/prometheus-operator/master/example/prometheus-operator-crd/podmonitor.crd.yaml
    printf "\n==> Install prometheus-operator chart \n"
    helm install --namespace observability --name prometheus-monitor -f playbooks/custom-values.yaml stable/prometheus-operator --set prometheusOperator.createCustomResource=false
    sleep 5
  else
    printf "==> Prometheus already installed \n"
  fi
}

deployIngress() {
  PWD=Pr0m4m1nPWD
  FILE=/tmp/auth
  htpasswd -b -c $FILE admin $PWD
  kubectl create secret generic admin --from-file $FILE -n observability
  rm $FILE
  kubectl apply -f playbooks/ingress.yaml
}

# main
installPrometheusOperator
deployIngress

exit 0
