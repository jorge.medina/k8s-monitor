#!/bin/bash

undeploy() {
  helm del --purge prometheus-monitor
}

# main
undeploy

exit 0
