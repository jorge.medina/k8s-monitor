#!/bin/bash

source ../extramodules/vars/allvars

generateMongoKey() {
  openssl rand -base64 741 > ./key.txt
  kubectl create secret generic $MONGO_SECRETS_VOLUME --from-file=internal-auth-mongodb-keyfile=./key.txt
}

deploy() {
  sed -i \
    -e "s^{{MONGO_IMG}}^$MONGO_IMG^g" \
    -e "s^{{MONGO_PORT}}^$MONGO_PORT^g" \
    -e "s^{{MONGO_APP_NAME}}^$MONGO_APP_NAME^g" \
    -e "s^{{MONGO_VOLUME_NAME}}^$MONGO_VOLUME_NAME^g" \
    -e "s^{{MONGO_MOUNT_PATH}}^$MONGO_MOUNT_PATH^g" \
    -e "s^{{MONGO_STORAGE_SIZE}}^$MONGO_STORAGE_SIZE^g" \
    -e "s^{{MONGO_SECRETS_VOLUME}}^$MONGO_SECRETS_VOLUME^g" \
    -e "s^{{MONGO_SECRETS_VOLUME_NAME}}^$MONGO_SECRETS_VOLUME_NAME^g" \
    -e "s^{{MONGO_SECRETS_VOLUME_PATH}}^$MONGO_SECRETS_VOLUME_PATH^g" \
  playbooks/deployment.yaml
  kubectl apply -f playbooks/deployment.yaml
}

# main
generateMongoKey
deploy

exit 0
