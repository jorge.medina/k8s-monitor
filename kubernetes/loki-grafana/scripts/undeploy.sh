#!/bin/bash

undeploy() {
  helm del --purge loki
  helm del --purge loki-grafana
}

# main
undeploy

exit 0
