#!/bin/bash

source ../extramodules/vars/allvars

installLoki() {
  CMD=$(helm ls --all 'loki' | wc -l)
  if [[ "$CMD" == 0 ]]; then
    printf "\n==> Add loki repo \n"
    helm repo add loki https://grafana.github.io/loki/charts
    helm repo update
    printf "\n==> Install loki \n"
    helm install --name loki \
      --namespace observability \
      --set loki.persistence.enabled=true \
      --set loki.persistence.size=50Gi \
      --set loki.persistence.storageClassName=gp2 loki/loki-stack
    sleep 5
  else
    printf "==> Loki already installed \n"
  fi
}

# main
installLoki

exit 0
