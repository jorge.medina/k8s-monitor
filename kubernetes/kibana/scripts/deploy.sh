#!/bin/bash

source ../extramodules/vars/allvars

replaceVars() {
  sed -i \
    -e "s^{{KIBANA_APP_NAME}}^$KIBANA_APP_NAME^g" \
    -e "s^{{KIBANA_PORT}}^$KIBANA_PORT^g" \
    -e "s^{{ELASTICSEARCH_URL}}^$ELASTICSEARCH_URL^g" \
    -e "s^{{KIBANA_HOST}}^$KIBANA_HOST^g" \
  playbooks/deployment.yaml \
  playbooks/ingress.yaml
}

deploy() {
  kubectl apply -f playbooks/deployment.yaml
  kubectl apply -f playbooks/ingress.yaml
}

# main
replaceVars
deploy

exit 0
