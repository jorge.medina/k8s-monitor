#!/bin/bash

# this script creates a basic auth for using into an ingress object.

FILE=/tmp/auth
SECRET=$1
PWD=$2

htpasswd -b -c $FILE $SECRET $PWD
kubectl create secret generic $SECRET --from-file $FILE
kubectl get secret $SECRET -o yaml
rm $FILE
