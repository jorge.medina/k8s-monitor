#!/bin/bash

source ../extramodules/vars/allvars

deploy() {
  sed -i \
    -e "s^{{ELASTIC_MOUNT_PATH}}^$ELASTIC_MOUNT_PATH^g" \
    -e "s^{{ELASTIC_STORAGE_SIZE}}^$ELASTIC_STORAGE_SIZE^g" \
  playbooks/deployment.yaml
  kubectl apply -f playbooks/deployment.yaml
}

# main
deploy

exit 0
